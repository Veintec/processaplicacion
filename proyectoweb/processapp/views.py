from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponse
from processapp.models import USUARIO
from django.http import JsonResponse
from django.core import serializers
from suds.client import Client
from django.contrib.auth import authenticate, logout
from django.contrib import auth
from django.utils import timezone
from datetime import datetime
from datetime import date
from datetime import timedelta
import datetime
from .clases import *
import requests
import sys
import json
from tkinter import messagebox
from django.views.decorators.csrf import csrf_protect
import csv
import pandas as pd
from fpdf import FPDF
# Create your views here.

# ---------------------------------------------- Vistas Web Pages
# Vistas pre login


def index(request):
    try:
        del request.session['USERNAME']
        del request.session['DESCRIPCION']
    except:
        pass
    return render(request, 'index.html')


def registro(request):
    return render(request, 'registro.html')


def login(request):
    return render(request, 'login.html')


def quienessomos(request):
    return render(request, 'quienessomos.html')

# ------------------------------------------Vistas Usuarios Logeados
# Vista Funcionario


def vistafuncionario(request):
    return render(request, 'vistafuncionario.html')


def progresofuncionario(request):
    return render(request, 'progresofuncionario.html')


def flujofuncionario(request):
    return render(request, 'flujofuncionario.html')


def notificacionfuncionario(request):
    return render(request, 'notificacionfuncionario.html')


def tareasfuncionario(request):
    return render(request, 'tareasfuncionario.html')

# Vista Funcionario Jefe


def vistafuncionariojefe(request):
    return render(request, 'vistafuncionariojefe.html')


def progresofuncionariojefe(request):
    return render(request, 'progresofuncionariojefe.html')


def notificacionfuncionariojefe(request):
    return render(request, 'notificacionfuncionariojefe.html')


def tablerofuncionariojefe(request):
    return render(request, 'tablerofuncionariojefe.html')

# Vista Administrador


def vistaadministrador(request):
    if request.session['DESCRIPCION'] == 'Admin_cliente':
        return render(request, 'vistaadministrador.html')
    else:
        return redirect('login')

# Vista Analista


def vistaanalista(request):
    # if request.session['DESCRIPCION'] == 'AnaliProcces':
    #     return render(request, 'vistaanalista.html')
    # else:
    #     return redirect('login')

    return render(request, 'vistaanalista.html')


# Vista Diseñador


def vistadisenador(request):
    return render(request, 'vistadisenador.html')


def departamentodisenador(request):
    return render(request, 'departamentodisenador.html')


def tareadisenador(request):
    return render(request, 'tareadisenador.html')

# -----------------------------Vistas Descartadas


def panelcontrol(request):
    return render(request, 'panelcontrol.html')


def sidepanel(request):
    return render(request, 'sidepanel.html')


def empresa(request):
    return render(request, 'empresa.html')


def flujotrabajo(request):
    return render(request, 'flujotrabajo.html')


def funcionario(request):
    return render(request, 'funcionario.html')


def personal(request):
    return render(request, 'personal.html')


def progreso(request):
    return render(request, 'progreso.html')


def tarea(request):
    return render(request, 'tarea.html')


# -------------------------- URL WCF ---------------------------

llamarurl = "http://localhost:59556/Service.svc?wsdl"

# ---------------------------- LOGIN -------------------------------
try:
    def autenticacion(request):
        USERNAME = request.POST.get('usuario', '')
        PASSWORD = request.POST.get('password', '')

        user = USUARIO(

            USERNAME=USERNAME,
            PASSWORD=PASSWORD
        )

        request.session['USERNAME'] = user.USERNAME

        jdata = json.dumps(user.__dict__)

        wsdl = llamarurl
        client = Client(wsdl)
        result = client.service.WSValidarUserContraseña(jdata)

        if(result == False):

            # return redirect('login')

            try:
                messages.MessageFailure("mensaje")
            except:
                messages.add_message
                (request,  messages.INFO, "Se ha producido una excepcion")
        else:

            rol = client.service.WSTraerRolCliente(jdata)

            rol1 = (json.loads(rol))

            request.session['DESCRIPCION'] = rol1

            if (rol1 == "Admin_cliente"):
                return redirect("vistaadministrador")
            elif (rol1 == "Disenador_cliente"):
                return redirect('vistadisenador')
            elif(rol1 == "Analista"):
                return redirect('vistaanalista')
            elif(rol1 == "Funcionario"):
                return redirect('tareasfuncionario')
            else:
                messages.MessageFailure("mensaje")
except Exception as e:
    print(str(e))


def login(request):
    try:
        del request.session['USERNAME']
        del request.session['DESCRIPCION']
    except:
        pass
    return render(request, 'login.html')


def CerrarSesion(request):
    try:
        del request.session['USERNAME']
        del request.session['DESCRIPCION']
        logout(request)
    except:
        pass
    return redirect('login')


@csrf_protect
def AgregarUsuario(request):

    try:
        if request.session['DESCRIPCION'] == 'Admin_cliente':

            wsdl = llamarurl
            client = Client(wsdl)

            USERNAME = request.POST.get('Username', '')
            PASSWORD = request.POST.get('Password', '')
            RUT = request.POST.get('rut', '')
            NOMBRE = request.POST.get('Nombre', '')
            ROL = request.POST.get('rol', '')
            CARGO = request.POST.get('cargo', '')
            EMAIL = request.POST.get('email', '')

            userempresa = USUARIO(
                USERNAME=request.session['USERNAME']
            )

            JdataUserEmp = json.dumps(userempresa.__dict__)

            usuario = USUARIO(
                USERNAME=USERNAME,
                PASSWORD=PASSWORD
            )

            JdataUsuario = json.dumps(usuario.__dict__)

            empleado = EMPLEADO(
                RUT=RUT,
                NOMBRE=NOMBRE,
                MAIL=EMAIL,
                ID_CARGO=CARGO,
                ID_ROL=ROL,
                USERNAME=USERNAME,
                ID_EMPRESA=client.service.WSTraerIdEmpresa(JdataUserEmp)
            )

            JdataEmpleado = json.dumps(empleado.__dict__)

            client.service.WSAgregarUsuario(JdataUsuario)

            client.service.WSAgregarEmpleado(JdataEmpleado)

            return redirect('vistaadministrador')
        else:
            return redirect(login)
    except:
        return redirect(login)


def mostrarUsuario(request):
    try:
        if request.session['DESCRIPCION'] == 'Admin_cliente':

            wsdl = llamarurl
            client = Client(wsdl)

            usuario = USUARIO(
                USERNAME=request.session['USERNAME']
            )

            jdatau = json.dumps(usuario.__dict__)

            emp = EMPRESA(
                ID_EMPRESA=int(client.service.WSTraerIdEmpresa(jdatau))
            )

            jdatae = json.dumps(emp.__dict__)

            traerempleado = (json.loads(
                client.service.WSListaEmpleadosUser(jdatau)))
            traercargo = (json.loads(
                client.service.WSListaCargoEmpresa(jdatae)))
            traerRol = (json.loads(client.service.WSListaRolCliente()))

            return render(request, 'vistaadministrador.html',  {"empleado": traerempleado, "cargo": traercargo, "rol": traerRol})
        else:
            return redirect(login)
    except:
        return redirect(login)


def agregartarea(request):
    try:
        if request.session['DESCRIPCION'] == 'Disenador_cliente':

            wsdl = llamarurl
            client = Client(wsdl)

            print('-----------------')

            NOMBRE_TAREA = request.POST.get('Nombre', '')
            DESCRIPCION_TAREA = request.POST.get('Descripcion', '')
            ID_FLUJO = request.POST.get('flujo', '')
            TAREA_ID_TAREA = request.POST.get('tarea', '')

            print(NOMBRE_TAREA,'---',TAREA_ID_TAREA)





            tarea = TAREA(
                NOMBRE_TAREA=NOMBRE_TAREA,
                DESCRIPCION_TAREA=DESCRIPCION_TAREA,
                ID_FLUJO=ID_FLUJO,
                TAREA_ID_TAREA=TAREA_ID_TAREA
            )

            jdata = json.dumps(tarea.__dict__)

            client.service.WSAgregarTarea(jdata)

            return redirect('tareadisenador.html')
        else:
            return redirect(login)
    except:
        return redirect(login)


def mostrarFlujoUnidadDisenado(request):
    if request.session['DESCRIPCION'] == 'Disenador_cliente':

        wsdl = llamarurl
        client = Client(wsdl)

        usuario = USUARIO(
            USERNAME=request.session['USERNAME']
        )

        jdatau = json.dumps(usuario.__dict__)

        traerflujo = (json.loads(client.service.WSListaFlujoUser(jdatau)))
        traerUnidad = (json.loads(client.service.WSListaUnidadUser(jdatau)))

        return render(request, 'vistadisenador.html',  {"flujo": traerflujo, "unidad": traerUnidad})
    else:
        return redirect('login')


def mostrarFlujoFuncionario(request):
    if request.session['DESCRIPCION'] == 'Funcionario':

        wsdl = llamarurl
        client = Client(wsdl)

        usuario = USUARIO(
            USERNAME=request.session['USERNAME']
        )

        jdata = json.dumps(usuario.__dict__)

        resultEmpleado = client.service.WSListaEmpleadosUser(jdata)

        emp = EMPRESA(
            ID_EMPRESA=int(client.service.WSTraerIdEmpresa(jdata))
        )

        emp = json.dumps(emp.__dict__)
        print(emp)

        resultadoFlujoEmpresa = client.service.WSListaFlujoEmpresa(emp)
        resultadoEmpleadoTarea = client.service.WSListaEmpleadoEmpresa(emp)

        ID_FLUJO = request.POST.get('idflujo', '')

        flujo = FLUJO(
            ID_FLUJO=1
        )

        flujo = json.dumps(flujo.__dict__)

        Resultadolistatarea = client.service.WSListaflujotarea(flujo)
        #resultListaFlujo = client.service.WSListaFlujo()
        resultListaUnidad = client.service.WSListaUnidad()

        traertarea = (json.loads(Resultadolistatarea))
        traerflujo = (json.loads(resultadoFlujoEmpresa))
        traerresponsable = json.loads(resultadoEmpleadoTarea)
        #traerListaflujo = (json.loads(resultListaFlujo))
        traerUnidad = (json.loads(resultListaUnidad))

        return render(request, 'flujofuncionario.html',  {"flujo": traerflujo, "unidad": traerUnidad, "tarea": traertarea, "responsable": traerresponsable})
    else:
        return redirect('login')


def Notificaciones(request):
    if request.session['DESCRIPCION'] == 'Funcionario':

        wsdl = llamarurl
        client = Client(wsdl)

        usuario = USUARIO(
            USERNAME=request.session['USERNAME']
        )

        jdata = json.dumps(usuario.__dict__)
        print(jdata)

        traerTareasEnviadas = json.loads(
            client.service.WSListaTareaEnviadas(jdata))

        return render(request, 'notificacionfuncionario.html', {"tareaenviadas": traerTareasEnviadas})
    else:
        return redirect('login')


def guardartarea(request):
    if request.session['DESCRIPCION'] == 'Funcionario':

        wsdl = llamarurl
        client = Client(wsdl)

        RUT = request.POST.get('rut', '')
        ID_TAREA = request.POST.get('idtarea', '')
        FECHA_INICIO_TAREA = request.POST.get('fechainicio', '')
        FECHA_TERMINO_TAREA = request.POST.get('fechatermino', '')

        empleado_tarea = EMPLEADO_TAREA(
            RUT=RUT,
            ID_TAREA=ID_TAREA,
            FECHA_INICIO_TAREA=FECHA_INICIO_TAREA,
            FECHA_TERMINO_TAREA=FECHA_TERMINO_TAREA,
            ID_ESTADO_TAREA=1
        )

        jdata = json.dumps(empleado_tarea.__dict__)

        AgregarEmpleadoTarea = client.service.WSAgregarEmpleadoTarea(jdata)

        return redirect("flujofuncionario")

    else:
        return redirect('login')


def justificacion(request):
    if request.session['DESCRIPCION'] == 'Funcionario':

        JUSTIFICACION = request.POST.get('justificacion', '')

        print(JUSTIFICACION)

        return render(request, 'notificacionfuncionario.html', {"justificacion": JUSTIFICACION})
    else:
        return redirect('login')


def GenerarReporte(request):

    wsdl = llamarurl
    client = Client(wsdl)

    archivojson = pd.read_json(client.service.WSListaTarea())
    archivojson.to_csv(
        'C:/Users/Ismael/Desktop/processprueba/proyectoweb/static/reporte.csv', index=None, header=True)

    with open('C:/Users/Ismael/Desktop/processprueba/proyectoweb/static/reporte.csv', encoding='utf-8') as f:
        reader = csv.reader(f)

        pdf = FPDF()
        pdf.add_page()
        page_width = pdf.w - 2 * pdf.l_margin

        pdf.set_font('Times', 'B', 14.0)
        pdf.cell(page_width, 0.0, 'Students Data', align='C')
        pdf.ln(10)

        pdf.set_font('Courier', '', 12)

        col_width = page_width / 4

        pdf.ln(1)

        th = pdf.font_size

        for row in reader:
            # print(row)
            pdf.cell(col_width, th, str(row), border=1)
            pdf.cell(col_width, th, row[1], border=1)
            pdf.cell(col_width, th, row[2], border=1)
            pdf.cell(col_width, th, row[3], border=1)
            pdf.cell(col_width, th, row[4], border=1)
            pdf.cell(col_width, th, row[5], border=1)
            pdf.ln(th)

        pdf.ln(10)

        pdf.set_font('Times', '', 10.0)
        pdf.cell(page_width, 0.0, '- end of report -', align='C')

        pdf.output('ReporteTarea.pdf', 'F')

        return redirect('progresofuncionario')


def cambiarestadotarea(request):
    if request.session['DESCRIPCION'] == 'Funcionario':

        wsdl = llamarurl
        client = Client(wsdl)

        RUT = request.POST.get('rut', '')
        ID_TAREA = request.POST.get('idtarea', '')
        FECHA_INICIO_TAREA = request.POST.get('fechainicio', '')
        FECHA_TERMINO_TAREA = request.POST.get('fechatermino', '')

        empleado_tarea = EMPLEADO_TAREA(
            RUT=RUT,
            ID_TAREA=ID_TAREA,
            FECHA_INICIO_TAREA=FECHA_INICIO_TAREA,
            FECHA_TERMINO_TAREA=FECHA_TERMINO_TAREA,
            ID_ESTADO_TAREA=3
        )

        jdata = json.dumps(empleado_tarea.__dict__)

        cambiarestado = client.service.WSModificarEstadoTarea(jdata)
        return redirect("notificacionfuncionario")

    else:
        return redirect('login')


def mostrarFlujoTareaDisenador(request):
    if request.session['DESCRIPCION'] == 'Disenador_cliente':

        wsdl = llamarurl
        client = Client(wsdl)

        usuario = USUARIO(
            USERNAME=request.session['USERNAME']
        )

        jdatau = json.dumps(usuario.__dict__)        

        traerflujo = (json.loads(client.service.WSListaFlujoUser(jdatau)))        

        return render(request, 'tareadisenador.html',  {"flujo": traerflujo})
    else:
        return redirect('login')


def mostrarTareadeFlujoDisenador(request):

    try:
        wsdl = llamarurl
        client = Client(wsdl)

        ID_FLUJO = request.POST.get('flujo')

        flujo = FLUJO(
            ID_FLUJO= ID_FLUJO
        )

        jdataf = json.dumps(flujo.__dict__)


        TraerTarea = json.loads(client.service.WSListaTareaDeFlujoSelected(jdataf))
        return JsonResponse(
            {
                'tareas':TraerTarea
            }
        )
        
    except Exception as ex:
        return JsonResponse(
            {
                'exception':str(ex)
            }
        )



# def cargar_tarea(request):
#     if request.session['DESCRIPCION'] == 'Disenador_cliente':
#         wsdl = llamarurl
#         client = Client(wsdl)
#         ID_FLUJO = request.POST.get('id_flujo' , '')
#         flujo = FLUJO(
#             ID_FLUJO= ID_FLUJO
#         )
#         jdata = json.dumps(flujo.__dict__)
#         TraerTarea = (json.loads(client.service.WSListaTareaDeFlujoSelected(jdata)))
#         return render(request, 'tareadisenador.html',  {"tarea": TraerTarea})
#     else:
#         return redirect('login')
try:
    def AgregarFlujo(request):

        wsdl = llamarurl
        client = Client(wsdl)

        usuario = USUARIO(
            USERNAME=request.session['USERNAME']
        )

        jdatau = json.dumps(usuario.__dict__)

        traerEmpresaUser = client.service.WSTraerIdEmpresa(jdatau)

        ID_FLUJO = int(request.POST.get('idflujo', ''))
        ID_EMPRESA = int(traerEmpresaUser)
        NOMBRE_FLUJO = request.POST.get('nombreflujo', '')

        FlujoEmpresa = FLUJO_EMPRESA(

            ID_FLUJO=ID_FLUJO,
            ID_EMPRESA=ID_EMPRESA,
            NOMBRE_FLUJO=NOMBRE_FLUJO
        )

        jdatafe = json.dumps(FlujoEmpresa.__dict__)

        flujo = FLUJO(
            ID_FLUJO=ID_FLUJO
        )
        empresa = EMPRESA(
            ID_EMPRESA=ID_EMPRESA
        )
        jdataf = json.dumps(flujo.__dict__)
        jdatae = json.dumps(empresa.__dict__)
        if client.service.WSValidarFlujoEnProceso(jdataf, jdatae) == False:

            result = client.service.WSAgregarFlujoEmpresa(jdatafe)

        return redirect("tareasfuncionario")

except Exception as e:
    print(str(e))


def ejecutarTareaFuncionario(request):

    if request.session['DESCRIPCION'] == 'Funcionario':

        wsdl = llamarurl
        client = Client(wsdl)

        resultListaTarea = client.service.WSListaTarea()
        ResultadoEstaSemana = client.service.WSListaTareaEstaSemana()
        ResultadoProximaSemana = client.service.WSListaTareaProximaSemana()
        ResultadoAtrasada = client.service.WSListaTareaAtrasadas()

        TraerTarea = (json.loads(resultListaTarea))
        TraerEstaSemana = (json.loads(ResultadoEstaSemana))
        TraerProximaSemana = (json.loads(ResultadoProximaSemana))
        TraerAtrasada = (json.loads(ResultadoAtrasada))

        return render(request, 'tareasfuncionario.html', {"tarea": TraerTarea, "FechaEstaSemana": TraerEstaSemana, "FechaProximaSemana": TraerProximaSemana, "FechaAtrasadas": TraerAtrasada})
    else:
        return redirect('login')

def agregarFlujoDiseñador(request):
    if request.session['DESCRIPCION'] == 'Disenador_cliente':

        return redirect("login")
    else:
        return redirect('login')


