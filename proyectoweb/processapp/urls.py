from django.urls import path
from django.conf.urls import url
from . import views



urlpatterns = [
    #-----------------------------Vistas pre login

    path('',views.index, name="index"),
    path('login/',views.login,name="login"),
    path('Contactanos/',views.quienessomos, name="Contactanos"),
    path('panelcontrol/',views.panelcontrol, name="panelcontrol"),

    #-------------------------Iniciar_Sesion
    path('login/perfil',views.autenticacion,name="autenticacion"),
    path('flujofuncionario/ejecutarflujo' , views.AgregarFlujo, name='EjecutarFlujo'),


    path('cerrarsession' , views.CerrarSesion , name='cerrasesion'),

    #-------------------------Nuevas usuarios logeados

    #Funcionario
    path('vistafuncionario/',views.tareasfuncionario, name="vistafuncionario"),
    path('progresofuncionario/',views.progresofuncionario, name="progresofuncionario"),
    path('progresofuncionario/Reporte',views.GenerarReporte,name='reporte'),
    path('notificacionfuncionario/',views.Notificaciones, name="notificacionfuncionario"),
    path('notificacionfuncionario/AceptarTarea',views.cambiarestadotarea,name="aceptartarea"),
    path('notificacionfuncionario/justificacion' ,views.justificacion,name="justificacion"),
    path('flujofuncionario/',views.mostrarFlujoFuncionario, name="flujofuncionario"),
    path('tareasfuncionario/',views.ejecutarTareaFuncionario, name="tareasfuncionario"),
    path('flujofuncionario/GuardarTarea',views.guardartarea, name="guardartarea"),

    #Administrador
    path('vistaadministrador/',views.mostrarUsuario, name="vistaadministrador"),
    path('vistaadministrador/AgregarUsuario' , views.AgregarUsuario , name='AgregarUsuario'),

    #Analista
    path('vistaanalista/',views.vistaanalista, name="vistaanalista"),
    
    #Diseñador
    path('vistadisenador/',views.mostrarFlujoUnidadDisenado, name="vistadisenador"),
    path('tareadisenador/',views.mostrarFlujoTareaDisenador, name="tareadisenador"),
    path('departamentodisenador/',views.departamentodisenador, name="departamentodisenador"),
    # path('vistadisenador/AgregarFlujo/' , views.agregarFlujoDiseñador , name="agregarflujo"),

    path('tareadisenador/AgregarTarea',views.agregartarea, name="AgregarTarea"),
    path('tareadisenador/mostrarTareadeFlujoDisenador',views.mostrarTareadeFlujoDisenador, name="tareadeflujo"),

    


    #path('tareadisenador/cargartarea',views.cargar_tarea,name='cargarTarea'),

    #Funcionario Jefe
    path('vistafuncionariojefe/',views.vistafuncionariojefe, name="vistafuncionariojefe"),
    path('progresofuncionariojefe/',views.progresofuncionariojefe, name="progresofuncionariojefe"),
    path('notificacionfuncionariojefe/',views.notificacionfuncionariojefe, name="notificacionfuncionariojefe"),
    path('tablerofuncionariojefe/',views.tablerofuncionariojefe, name="tablerofuncionariojefe"),

    #--------------------------Vistas Descartadas
    # path('registro/',views.registro,name="registro"),
    # path('sidepanel/',views.sidepanel, name="sidepanel"),
    # path('login/funcio/',views.autenticacion,name="autenticacion"),
    # path('empresa/',views.empresa, name="empresa"),
    # path('flujotrabajo/',views.flujotrabajo, name="flujotrabajo"),
    # path('funcionario/',views.funcionario, name="funcionario"),
    # path('personal/',views.personal, name="personal"),
    # path('progreso/',views.progreso, name="progreso"),
    # path('tarea/',views.tarea, name="tarea"),    
]
