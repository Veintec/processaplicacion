$(document).ready(function () {
    $("#login").validate({
        rules: {
            usuario: {
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            usuario: {
                required: "Ingrese un usuario"
            },
            password: {
                required: "Ingrese contraseña"
            }
        }
    });
})

$("#login").submit(function () {
    if ($("#login").valid()) {
        return true
    } else {
        Swal.fire({
            type: 'info',
            text: 'Los Campos estan vacios',
        })
    }
    return false
})

$(document).ready(function () {
    $('#agregarusuario').validate({
        rules: {
            Username: {
                required: true
            },
            Password: {
                required: true
            },
            Nombre: {
                required: true
            },
            rut: {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            Username: {
                required: "Ingrese un Username"
            },
            Password: {
                required: "Ingrese una Contraseña"
            },
            Nombre: {
                required: "Ingrese su nombre"
            },
            rut: {
                required: "Ingrese su Rut"
            },
            email: {
                required: "Ingrese un correo electronico",
                email: "Debe ingresar un correo Valido"
            }

        }
    })
})

$("#agregarusuario").submit(function () {
    if ($("#agregarusuario").valid()) {
        return true
    } else {
        Swal.fire({
            type: 'info',
            text: 'Los Campos estan vacios',
        })
    }
})

$(document).ready(function () {
    $("#agregarflujodisenador").validate({
        rules: {
            NombreFlujo: {
                required: true
            },
            Fecha_Inicio: {
                required: true
            },
            Fecha_Termino: {
                required: true
            }
        },
        messages: {
            NombreFlujo: {
                required: "Ingrese un usuario"
            },
            Fecha_Inicio: {
                required: "Ingrese Fecha de Inicio"
            },
            Fecha_Termino: {
                required: "Ingrese Fecha de Termino"
            }

        }
    });
})

$("#agregarflujodisenador").submit(function () {
    if ($("#agregarflujodisenador").valid()) {
        return true
    } else {
        Swal.fire({
            type: 'info',
            text: 'Los Campos estan vacios',
        })
    }
    return false
})

$(document).ready(function () {
    $("#agregartarea").validate({
        rules: {
            NombreTareaDisenador: {
                required: true
            },
            DescripcionTareaDisenador: {
                required: true
            }
        },
        messages: {
            NombreTareaDisenador: {
                required: "Ingrese un Nombre"
            },
            DescripcionTareaDisenador: {
                required: "Ingrese Descripcion"
            }
        }
    });
})

$("#agregartarea").submit(function () {
    if ($("#agregartarea").valid()) {
        return true
    } else {
        Swal.fire({
            type: 'info',
            text: 'Los Campos estan vacios',
        })
    }
    return false
})
